﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationObj : MonoBehaviour
{
    [SerializeField] float speed;
    private Quaternion angle;

    private void Start()
    {
        angle = Quaternion.Euler(0.0f, speed, 0.0f);
    }

    private void FixedUpdate()
    {
        transform.rotation *= angle;
    }
}
