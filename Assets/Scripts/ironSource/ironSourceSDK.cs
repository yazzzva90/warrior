﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ironSourceSDK : MonoBehaviour
{
    private static ironSourceSDK _instance;
    public static ironSourceSDK Instance { get { return _instance; } }

    [SerializeField] string APP_KEY;

    private void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        _instance = this;
    }

    private void Start()
    {
        IronSource.Agent.init(APP_KEY);
        ShowBanner();
    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }

    public void ShowBanner()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.TOP);
    }

    public void DestroyBanner()
    {
      //  IronSource.Agent.destroyBanner();
    }
}
