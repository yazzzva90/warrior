﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator animator;
    private float timer = 5.0f;

    private void Start()
    {
        animator = GetComponent<Animator>();
        enabled = false;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0.0f)
        {
            animator.SetTrigger("shoot");
            PlayerController.Instance.Kill();
            enabled = false;
        }
    }

    public void Kill()
    {
        animator.SetTrigger("dead");
        enabled = false;
    }

    public void Activate()
    {
        timer = 0.6f;
        enabled = true;
    }
}
