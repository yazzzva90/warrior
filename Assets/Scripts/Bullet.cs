﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float speedAnimRotation = 20.0f;

    private Rigidbody rigidbody;
    private Vector3 direction;


    internal void SetDirection(Vector3 target)
    {
        GetComponent<SphereCollider>().enabled = true;
        rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        rigidbody.useGravity = false;
        rigidbody.velocity = (target - transform.position).normalized * 20.0f;
    }

    private void Update()
    {

        //   Vector3 rotation = transform.rotation.eulerAngles;
        //  rotation.y += speedAnimRotation * Time.deltaTime;
        transform.GetChild(0).Rotate(0.0f, speedAnimRotation, 0.0f, Space.Self);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Destroy(gameObject);
            other.gameObject.GetComponent<Enemy>().Kill();
        }
    }
}
