﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private Animator animator;
    private float gravity = 7.0f;
    private float speed = 3.0f;
    private Vector3 moveDirection = Vector3.zero;
    private bool jumpState = false;

    private TriggerState state;
    private Ragdoll ragdoll;

    private static PlayerController instance;

    public static PlayerController Instance { get { return instance; } }

    private void Start()
    {
        instance = this;
        ragdoll = GetComponent<Ragdoll>();
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        animator.SetTrigger("run");
        moveDirection.x = speed;
    }

    private void Update()
    {

        if (!jumpState)
            moveDirection.y = -1.0f;
        else
            moveDirection.y -=  gravity * Time.deltaTime;

            controller.Move(moveDirection * Time.deltaTime);
         
    }

    private void OnTriggerEnter(Collider other)
    {
        state = other.gameObject.GetComponent<TriggerState>();
        if (state != null)
        {
            animator.SetTrigger("jump");
            jumpState = true;
            StartCoroutine(TimeScale(0.4f, 0.5f));
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("WIN"))
        {
            CanvasManager.Instance.WIN();
            FacebookEvent.Instance.LogAchieveLevelEvent("level completed");
        }
    }

    public void Jump()
    {
        moveDirection.y = state.jumpForce;
        moveDirection.x = state.jumpSpeed;
        gravity = state.gravity;
    }
    public void EndJump()
    {
        moveDirection.x = speed;
        jumpState = false;
        StartCoroutine(TimeScale(1.0f, 0.5f));
    }

    public void Kill()
    {
        ragdoll.Activate();
        CanvasManager.Instance.GameOver();
    }

    private IEnumerator TimeScale(float timeScale, float uploadTime)
    {
        float timer = 0.0f;
        float distCurved = 0.0f;
        float difTimeScale = timeScale - Time.timeScale;
        float saveTimeScale = Time.timeScale;

        while(distCurved < 1.0f)
        {
            distCurved = Mathf.Clamp01(timer / uploadTime);
            Time.timeScale = saveTimeScale + difTimeScale * distCurved;
            timer += Time.deltaTime;
            yield return null;
        }

        Time.timeScale = timeScale;
    }
}
