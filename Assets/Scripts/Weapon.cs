﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] GameObject hammer;
    private Vector3 target;
    private bool shoot = false;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Shoot(Vector3 target)
    {
        if (shoot) return;
        shoot = true;
        //  animator.SetTrigger("atack");
        this.target = target;
        Throw();
        
    }

    public void Throw()
    {
        GameObject obj = Instantiate(hammer, hammer.transform.position, hammer.transform.rotation);
        obj.AddComponent<Bullet>().SetDirection(target);
        hammer.SetActive(false);
        StartCoroutine(Reload(0.15f));
    }

    private IEnumerator Reload(float timer)
    {
        yield return new WaitForSeconds(timer);
        hammer.SetActive(true);
        shoot = false;
    }
}
