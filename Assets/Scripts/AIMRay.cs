﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMRay : MonoBehaviour
{
    [SerializeField] LineRenderer line;
    private Transform aim;
    private Weapon weapon;

    private void Start()
    {
        weapon = GetComponent<Weapon>();
        aim = line.transform;
        line.enabled = false;
        enabled = false;
    }

    private void Update()
    {
        RaycastHit hit;
        Debug.DrawRay(aim.position, aim.TransformDirection(Vector3.up));
        // Does the ray intersect any objects excluding the player layer
        int layerMask = 1 <<10;
        layerMask = ~layerMask;
        Vector3 target;
        if (Physics.Raycast(aim.position, aim.TransformDirection(Vector3.up), out hit, Mathf.Infinity, layerMask))
        {
            line.SetPosition(1, new Vector3(0.0f, hit.distance, 0.0f));
           if(hit.transform.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                hit.transform.gameObject.GetComponent<Enemy>().Activate();
            }
            target = hit.point;
        }
        else
        {
            line.SetPosition(1, new Vector3(0.0f, 20.0f, 0.0f));
            target = aim.position + aim.TransformDirection(Vector3.up) * 20.0f;
        }

        if (Input.GetMouseButton(0))
            weapon.Shoot(target);

    }
    public void OnAIM()
    {
        line.enabled = true;
        enabled = true;
    }
    public void OffAIM()
    {
        line.enabled = false;
        enabled = false;
    }
}
