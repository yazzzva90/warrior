﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] Text text;
    private Canvas canvas;

    private static CanvasManager instance;

    public static CanvasManager Instance { get { return instance; } }
    private void Awake()
    {
        Time.timeScale = 1.0f;
        instance = this;
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    public void GameOver()
    {
        StartCoroutine(OpenCanvas(2.0f, "GAME OVER", Color.red));
    }
    public void WIN()
    {
        StartCoroutine(OpenCanvas(0.0f, "YOU WIN", Color.green));
    }

    private IEnumerator OpenCanvas(float time, string txt, Color color)
    {
        yield return new WaitForSeconds(time);

        Time.timeScale = 0.0f;
        text.text = txt;
        text.color = color;
        canvas.enabled = true;
    }

    public void Reload()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
