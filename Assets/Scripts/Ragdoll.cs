﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    private Rigidbody[] rigidbodies;
    private CharacterController characterController;
    private Animator animator;
    private AIMRay aim;

    private void Start()
    {
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        aim = GetComponent<AIMRay>();

        foreach(Rigidbody _rig in rigidbodies)
        {
            _rig.isKinematic = true;
        }
    }
    
    public void Activate()
    {
      
      
        foreach (Rigidbody _rig in rigidbodies)
        {
            _rig.isKinematic = false;
            _rig.interpolation = RigidbodyInterpolation.Interpolate;
            _rig.AddForce(characterController.velocity * 40.0f);
        }
        characterController.enabled = false;
        animator.enabled = false;
        aim.OffAIM();
    }




}
