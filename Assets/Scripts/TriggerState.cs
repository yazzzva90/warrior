﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerState : MonoBehaviour
{
    public float jumpForce;
    public float gravity;
    public float jumpSpeed;
}
